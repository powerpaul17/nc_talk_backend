export enum RoomApiRequestType {
  INVITE = 'invite',
  DISINVITE = 'disinvite',
  UPDATE = 'update',
  DELETE = 'delete',
  PARTICIPANTS = 'participants',
  INCALL = 'incall',
  MESSAGE = 'message'
}

export type RoomApiRequest = IRoomApiInviteRequest |
  IRoomApiDisinviteRequest |
  IRoomApiUpdateRequest |
  IRoomApiDeleteRequest |
  IRoomApiParticipantsRequest |
  IRoomApiIncallRequest |
  IRoomApiMessageRequest;

interface IRoomApiInviteRequest {
  type: RoomApiRequestType.INVITE,
  invite: IRoomApiInviteRequestData
}

interface IRoomApiDisinviteRequest {
  type: RoomApiRequestType.DISINVITE,
  disinvite: RoomApiDisinviteRequestData
}

interface IRoomApiUpdateRequest {
  type: RoomApiRequestType.UPDATE,
  update: IRoomApiUpdateRequestData
}

interface IRoomApiDeleteRequest {
  type: RoomApiRequestType.DELETE,
  delete: IRoomApiDeleteRequestData
}

interface IRoomApiParticipantsRequest {
  type: RoomApiRequestType.PARTICIPANTS,
  participants: IRoomApiParticipantsRequestData
}

interface IRoomApiIncallRequest {
  type: RoomApiRequestType.INCALL,
  incall: IRoomApiIncallRequestData
}

interface IRoomApiMessageRequest {
  type: RoomApiRequestType.MESSAGE,
  message: IRoomApiMessageRequestData
}



export interface IRoomApiInviteRequestData {
  userids: Array<string>,
  alluserids: Array<string>,
  properties: IRoomApiRoomProperties
}

export type RoomApiDisinviteRequestData = RoomApiDisinviteUserIdsRequestData | RoomApiDisinviteSessionIdsRequestData;

interface IRoomApiDisinviteRequestDataCommon {
  alluserids: Array<string>,
  properties: IRoomApiRoomProperties
}

type RoomApiDisinviteUserIdsRequestData = IRoomApiDisinviteRequestDataCommon & {
  userids: Array<string>
};

type RoomApiDisinviteSessionIdsRequestData = IRoomApiDisinviteRequestDataCommon & {
  sessionids: Array<string>
};

export interface IRoomApiUpdateRequestData {
  userids: Array<string>,
  properties: IRoomApiRoomProperties
}

export interface IRoomApiDeleteRequestData {
  userids: Array<string>
}

export interface IRoomApiParticipantsRequestData {
  changed: Array<IParticipant>,
  users: Array<IParticipant>
}

export interface IRoomApiIncallRequestData {
  incall: number, // TODO: in call flags
  changed: Array<IParticipant>,
  users: Array<IParticipant>
}

export interface IRoomApiMessageRequestData {
  data: any // TODO { chat: { refresh: true } }
}



interface IRoomApiRoomProperties {
  name: string,
  type: RoomType,
  'lobby-state': LobbyState,
  'lobby-timer': IDateObject|null,
  'read-only': ReadOnly,
  'active-since': IDateObject|null
}

declare enum RoomType {
  UNKNOWN_CALL = -1,
  ONE_TO_ONE_CALL = 1,
  GROUP_CALL,
  PUBLIC_CALL,
  CHANGELOG_CONVERSATION
}

declare enum LobbyState {
  NO = 0,
  YES
}

declare enum ReadOnly {
  READ_WRITE = 0,
  READ_ONLY
}



export interface IParticipant {
  inCall: number, // Combination of ParticipantFlags
  lastPing: number,
  sessionId: string, // NC session id
  participantType: ParticipantType,
  userId: string | undefined // for guests
}

declare enum ParticipantType {
  OWNER = 1,
  MODERATOR,
  USER,
  GUEST,
  USER_SELF_JOINED,
  GUEST_MODERATOR
}

declare enum ParticipantFlags {
  DISCONNECTED = 0,
  IN_CALL = 1,
  WITH_AUDIO = 2,
  WITH_VIDEO = 4
}



interface IDateObject {
  date: string,
  timezone: string,
  // eslint-disable-next-line camelcase
  timezone_type: number
}
